This repository is an inventory to create an instance of SomOffice for Comunitat Energetiques.

Uses the same strategy that [`odoo-provisioning`]():

* One repository with the Ansible playbooks and roles (tasks).
* N repositories with the Ansible inventory (hosts and variables).

## Setup

```sh
git clone git@gitlab.com:coopdevs/somoffice-provisioning.git
cd somoffice-provisioning
pyenv virtualenv 3.7.4 somoffice-provisioning
pyenv exec pip install -r requirements.txt
pyenv exec ansible-galaxy install -r requirements.yml
```

*TODO: Update Python version*

## Development

* install devenv https://github.com/coopdevs/devenv
* run the following

```sh
    cd /path/to/oficina-virtual-ce-inventory
    devenv
    cd /path/to/somoffice-provisioning
    pyenv exec ansible-playbook playbooks/sys_admins.yml --limit=dev -u root --ask-vault-pass -i /path/to/oficina-virtual-ce-inventory
    pyenv exec ansible-playbook playbooks/provision.yml --limit=dev --ask-vault-pass -i /path/to/oficina-virtual-ce-inventory
```

> Password in BW: `somoffice-ccee-provisioning dev ansible secrets`
